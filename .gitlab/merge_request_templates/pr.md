### Checklist review #PR

- [ ] Check if/else hạn chế sử dụng (tối đa 1) 
- [x] Check để từ, số cố định trong code 
- [x] Đặt tên biến, hàm đúng mục đích cần làm 
- [x] 1 function nên làm đúng 1 chức năng (ko để một function quá dài) 
- [ ] Khai báo hằng số thay cho var | let 
- [ ] Đồng bộ tên git repo 
- [ ] Nên viết code có thể tái sử dụng 
- [ ] Nên chú ý đến việc mở rộng khi cập nhập một giá trị, function
